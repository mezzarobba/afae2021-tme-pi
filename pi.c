/* Compilation :
 * gcc pi.c -lgmp -Wall -o pi
 */

#include <assert.h>
#include <stdio.h>
#include <gmp.h>

static mpz_t a, b, c, c3;

void init_constants (void) {
    mpz_init_set_ui(a, 545140134);
    mpz_init_set_ui(b, 13591409);
    mpz_init_set_ui(c, 640320);
    mpz_init(c3);
    mpz_pow_ui(c3, c, 3);
}

void clear_constants (void) {
    mpz_clear(c3);
    mpz_clear(c);
    mpz_clear(b);
    mpz_clear(a);
}

void f(mpz_t res, unsigned long n) {
    mpz_mul_ui(res, a, n);
    mpz_add(res, res, b);
}

/* On a
 *
 *   t(n+1)/t(n) = -(8(6n+1)(6n+3)(6n+5)/(n+1)³)(f(n+1)/f(n))(1/c³)
 *
 * où
 *
 *   f(n) = a*n+b.
 */

void term_ratio (mpz_t num, mpz_t den, unsigned long n) {

    mpz_t tmp;

    f(num, n+1);
    for (int k = 0; k < 3; ++k)
        mpz_mul_ui(num, num, 6*n + 2*k + 1);
    mpz_mul_2exp(num, num, 3);
    mpz_neg(num, num);

    f(den, n);
    mpz_init(tmp);
    mpz_ui_pow_ui(tmp, n + 1, 3);
    mpz_mul(den, den, tmp);
    mpz_clear(tmp);

    mpz_mul(den, den, c3);
}

typedef struct {
    mpz_t t, s, q;
} mat[1];

void mat_init (mat m) {
    mpz_init(m->t);
    mpz_init(m->s);
    mpz_init(m->q);
}

void mat_clear (mat m) {
    mpz_clear(m->t);
    mpz_clear(m->s);
    mpz_clear(m->q);
}

void mat_mult (mat res, mat high, mat low) {
    mpz_mul(res->t, high->t, low->t);
    mpz_mul(res->s, high->q, low->s);
    mpz_addmul(res->s, high->s, low->t);
    mpz_mul(res->q, high->q, low->q);
}

void binsplit (mat res, unsigned long i, unsigned long j) {
    assert (j > i);
    if (j == i + 1) {
        term_ratio(res->t, res->q, i);
        mpz_set(res->s, res->q);
    } else {
        mat low, high;
        mat_init(low);
        mat_init(high);
        unsigned long m = (i + j)/2;
        binsplit(low, i, m);
        binsplit(high, m, j);
        mat_mult(res, high, low);
        mat_clear(high);
        mat_clear(low);
    }
    /*
    if (i == 0) {
        mpq_t quo;
        mpq_init(quo);
        mpz_set(mpq_numref(quo), res->s);
        mpz_set(mpq_denref(quo), res->q);
        mpq_canonicalize(quo);
        gmp_printf("j=%u, S/Q=%Qd\n", j, quo);
        mpq_clear(quo);
    }
    */
}

void pi (mpf_t res, unsigned long prec) {

    mat sum;
    mat_init(sum);
    mpf_t tmp;
    mpf_init2(tmp, prec);
    mpz_t num, den;
    mpz_init(num);
    mpz_init(den);

    unsigned long nterms = prec/47 + 1; // 47 <= -3*log2(12/c)
    binsplit(sum, 0, nterms);

    unsigned long divprec = mpz_sizeinbase(sum->s, 2);
    mpf_t numf, denf;
    mpf_init2(numf, divprec);
    mpf_init2(denf, divprec);
    mpf_set_z(numf, sum->s);
    mpf_set_z(denf, sum->q);
    mpf_div(res, denf, numf);
    mpf_clear(denf);
    mpf_clear(numf);

    mpf_set_z(tmp, c);
    mpf_mul(res, res, tmp);

    mpf_sqrt(tmp, tmp);
    mpf_mul(res, res, tmp);

    mpf_div_ui(res, res, 12);

    mpf_set_z(tmp, b);
    mpf_div(res, res, tmp);

    mpz_clear(den);
    mpz_clear(num);
    mpf_clear(tmp);
    mat_clear(sum);
}

int main (void) {
    init_constants();

    unsigned long prec = 3010300;
    /* unsigned long prec = 1000; */

    mpf_t mypi;
    mpf_init2(mypi, prec);
    fprintf(stderr, "starting...\n");
    pi(mypi, prec);
    fprintf(stderr, "done!\n");
    gmp_printf("%.*Ff\n", (size_t) (0.301029995663981*prec), mypi);
    mpf_clear(mypi);

    clear_constants();
    return 0;
}
